# An example scRNAseq pipeline

In this tutorial you will run mapping, some qc-steps and gene expression estimation for scRNAseq data. We have set up a small example with 10 human embryonic cells from the study Petropoulos et al. (Cell 2016) [link](http://www.cell.com/cell/fulltext/S0092-8674(16)30280-X). These cells are SmartSeq2 libraries.

We have implemented a Snakemake pipline, but any type of pipeline language could perform these tasks. If you want to learn more about Snakemake, which is a python based pipeline language, please check their [documentation](http://snakemake.readthedocs.io/en/stable/) or run their [tutorial](http://snakemake.readthedocs.io/en/stable/tutorial/basics.html).

We have used Conda package management for all package installations, this way you can run the same pipeline on any system. For more information on Conda, please have a look at their [website](https://conda.io/docs/index.html).

We have prepared all installations already, but below is a short summary of the steps required to set up the pipeline. Read through the details, but start preparing your own folder and run mapping for a few samples from the step - "Create your own project folder".

## Conda installation and creating an environment - OBS! no need to run.

This has already been prepared for this course, but in case you want to run it yourself, you should follow these steps but changing paths etc. 

First, you need to install miniconda following instructions at: http://conda.pydata.org/docs/install/quick.html for your system.

At uppmax:

	cd /proj/b2013006/nobackup/scrnaseq_course/pipeline/conda
   	wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
   	bash Miniconda3-latest-Linux-x86_64.sh
   	# reply "yes" to licence agreement, and specify location for installation
   	# in this case, installed at: /proj/b2013006/nobackup/scrnaseq_course/pipeline/conda
   

Since the miniconda installation is at `/proj/b2013006/nobackup/scrnaseq_course/pipeline/conda` you will have to add that to your path. Either through your .bashrc or on the commandline.

      export PATH=/proj/b2013006/nobackup/scrnaseq_course/pipeline/conda/bin:$PATH

Next step is to create a conda environment, we have a file with all the dependencies for the pipeline at `requirements_scrnaseq_course.yaml` that specifies all the packages we want to install and what different channels to use to locate them, please have a look at the file to see how it is set up. We define a name for the environment, which will be `scrnaseq_conda` in this case.
     
     conda env create -n "scrnaseq_conda" -f /proj/b2013006/nobackup/scrnaseq_course/conda/requirements_scrnaseq_course.yaml

Now we have an environment with all the packages we need for running the pipeline. So to get access to all the programs, we need to activate the environment:

    source activate scrnaseq_conda

Now the name of the activated conda should appear in your terminal like:

    (scrnaseq_conda) [username@milou1 foldername]$
    
You should now be able to run all the programs like python, R, samtools, STAR etc using all programs installed under that conda environment.

## Install some additional R-dependencies - OBS! no need to run.

There are a few R-dependencies that are not avialable as condas, so these are installed via a script `install_R_packages.R`. There you have to specify the lib path to the correct environmet folder.

      export PATH=/proj/b2013006/nobackup/scrnaseq_course/pipeline/conda/bin:$PATH
      source activate scrnaseq_conda
      Rscript install_R_packages.R

## Cloning and installing the pipeline - OBS! no need to run.

The scrnaseq pipeline is located at: https://bitbucket.org/scilifelab-lts/lts-workflows-sm-scrnaseq

    cd /proj/b2013006/nobackup/scrnaseq_course/pipeline/
    git clone https://bitbucket.org/scilifelab-lts/lts-workflows-sm-scrnaseq.git


Now we will install the development version of the pipeline

    export PYTHONPATH="/proj/b2013006/nobackup/scrnaseq_course/pipeline/conda/envs/scrnaseq_course/lib/python3.5/site-packages"
    cd lts-workflows-sm-scrnaseq/
    git checkout develop   
    python setup.py develop

OBS! There is a conda version of the pipeline, and it should be possible to install the pipeline via conda instead, but we are now using the development version.

For now, we also need to create another conda for python2.7 since the Rseqc package requires python2.7.

    conda env create -n "python-27" -f lts_workflows_sm_scrnaseq/environment-27.yaml


## Download reference files - OBS! no need to run.

To run the snakemake pipeline, you need a number of files:

   * Genome file + ERCC spike-ins (+ any marker genes that you want to include, e.g. GFP, dsRed etc)
   * Annotation file + annotations for spike-ins + extra constructs
   * A bed-file with known genes for rseqc - suggested to download from Rseqc website for standard species.

In this example we will map data from a human embryonic study, so we need to get genome and annotation files for the human genome, here we use ensembl annotations/genome:

	 cd /proj/b2013006/nobackup/scrnaseq_course/pipeline/ref/
	 wget ftp://ftp.ensembl.org/pub/release-90/fasta/homo_sapiens/dna/Homo_sapiens.GRCh38.dna.primary_assembly.fa.gz
	 wget ftp://ftp.ensembl.org/pub/release-90/gtf/homo_sapiens/Homo_sapiens.GRCh38.90.gtf.gz	 
	 cp /proj/b2013006/nobackup/asab/data/genomes/ERCC/ERCC_spikes_no_polyA.fa ERCC.fasta
	 # unzip the files
	 gunzip Homo_sapiens.GRCh38.dna.primary_assembly.fa.gz
	 gunzip Homo_sapiens.GRCh38.dna.primary_assembly.fa.gz

OBS! If you download the ERCC from any other source, please make sure that you remove the polyA tails that are added to the file since they will give rise to spurios mapping of reads with long stretches of Ts.

You can also add in additional sequence files with marker genes, like EGFP, dsRed or similar that you want to include mapping for.

For rseqc we also need a more conservtive annotation file with only house-keeping genes, to speed up the time required for running the QC-steps.

# Files needed to run the pipeline

## The metadata file

The metadata file is a comma separated file with all possible information about the samples. The first column has to be sample name, with header `SM`. Some columns have to specify the folder names for where the files are located.

OBS! in the configuration file, you will have to define what the layout of the folders are, so make sure that the headers of the meta data file has the same names as they are defined in the sections `samplefmt` and `runfmt` in the configuration file.

We have a file prepared for the example with 10 samples, have a look at the file:
   
	less /proj/b2013006/nobackup/scrnaseq_course/pipeline/Meta_data_human_embryo.csv

## The Snakefile

The snakefile is the main workflow file that snakemake runs on. If there is a file named `Snakefile` in the folder you are standing in when you execute the command `snakemake` it will automatically detect the snakefile. But you can also use any name you want for the file, but then you would have to specify what file to run on with the flag 

In the snakefile you can write your snakemake rules directly, or load them from other sources, which we do in this example. So you will not need to change anything in the snakefile.

Have a look at the snakefile you will be running on:

     less /proj/b2013006/nobackup/scrnaseq_course/pipeline/Snakefile

## The configuration file

Snakemake configuration files can be in YAML format or JSON format. In this example we will use YAML format, for more information on YAML, please have a look at: https://learnxinyminutes.com/docs/yaml/.

OBS! Keep in mind that tabs in the wrong places of a YAML file can easily create errors, so use spaces for indented blocks instead of tabs.

In the configuration file you will have to specify the paths to your files, what files to include and you can also change the settings on what to run, on which samples to run it on, and how to set different parameters for all the programs that are used.

Define the format for input data (often you may have nested folders for the fastq files, and sometimes multiple fastq files for one sample) under the section `runformat`. And the format you want to use to use for the output data under `samplefmt`. In our case we have the same for both:

       samplefmt: data/samples/{SM}/{SM}
       runfmt: data/samples/{SM}/{SM}

Which corresponds to the folder structure that we have:

      data/samples/ERR1042421/ERR1042421.fastq.gz


Have a look at the configuration file:

     less /proj/b2013006/nobackup/scrnaseq_course/pipeline/config.yaml

# Rulegraph - summary of the pipeline

We can visualize all the rule dependencies in snakemake with a DAG using the `--rulegraph` option. We ran the command:

       snakemake make_qc_report --rulegraph -npF -d ./ --configfile config.yaml  | dot -Tpdf > rulegraph.pdf

Which created this plot:

![](files/rulegraph.png)

Here you can see all the steps that the pipeline wants to perform, and how they depend on eachother. Each arrow represents files that one rule creates and another rule has as input.

What will be done is:


* dbutils_fasta_to_genbank - creates a genbank file for ERCC or other extra seqs
* dbutils_make_transcript_annot_gtf - makes a joint gtf file for all sequences
* ucsc_gtfToGenepred - converts gtf to genepred
* ucsc_genepred_to_refFlat - converts genepred to refFlat - annotation file needed for rpkmforgenes
* rpkmforgenes_from_bam - runs rpkmforgenes to get gene expression
* expression_qc_stats - calculates some stats like detected genes/spike-ins, cell-to-cell correlations etc.
* make_count_rpkm_matrix - takes all individual rpkmforgenes outputs and creates one matrix.
* star_index - takes genome fasta and annotation gtf files and makes a STAR index
* star_align_se - alings fastq files to the star_index
* samtools_sort - will sort the bamfile
* samtools_index - creates a samtools index file
* symlink_baifile - since picard creates an index file with name sample.bam but RseQC requires sample.bam.bai, this rule creates a symlink to sample.bai with that name.
* rseqc_read_distribution - RseQC function that looks at where the reads map (exons, introns, igs etc)
* rseqc_geneBody_coverage - RseQC function that looks at where in the genes the reads map.
* rseqc_read_GC - RseQC function that looks at GC content of mapped reads
* multiqc - summarizes qc-stats for all rseqc steps and STAR alignment log
* merge_all_qc - makes one large table with all qc-stats
* make_qc_report - will create a suggested filtering and plot a bunch of QC-stats.


# Create your own project folder

First, go into your own folder and create one pipeline folder. Move into that folder and instead of copying all the large files that you need you will instead make symlinks with the command `cp -rs` that will create all the nested folders, but creates symlinks to the fastq files. We will also make a symlink to the reference folder with `ln -s` so that we do not have to make a copy and create indexes for the human genome.

       mkdir data
       cp -rs /proj/b2013006/nobackup/scrnaseq_course/pipeline/rawdata data/samples
       # make symlink to the reference folder
       ln -s /proj/b2013006/nobackup/scrnaseq_course/pipeline/ref data/ref
       # copy the configuraiton file
       cp /proj/b2013006/nobackup/scrnaseq_course/pipeline/config.yaml .
       # copy the Snakemake file
       cp /proj/b2013006/nobackup/scrnaseq_course/pipeline/Snakefile .

Clone the qc-summary scripts to your own folder. This repo has to be cloned into your personal directory since rmarkdown will create intermediate spin files in the folder where the script is, so you need to have a private copy of the script in your folder.

     git clone https://bitbucket.org/asbj/qc-summary_scrnaseq.git 

OBS! Make sure to set the correct path to the qc-summary folder in the last section of the configuration file, under `qc_settings´ and entry `render_report_script`.

Just open the `config.yaml` with your favourite text editor (emacs, vim, nano or similar) and modify the neccesary lines.

# Run the pipeline

For running different steps of the pipeline there are a number of target rules that specifies which main steps of the pipeline that should be run:

* align - will run STAR mapping
* rseqc - will run all steps of the RseQC package
* rpkmforgenes - will run rpkmforgenes to estimate gene expression
* all - will run all steps above
* make_qc_report - will also prepare summary files and make qc summary - with this target rule everything in the pipeline should run. 

With snakemake, it is a good idea to start with a dryrun (using flag -n) to check that everything looks okay. Run also with flag -p to print out all the commands that snakemake wants to execute. 

What you have to specify is the flags:

* -d - for the directory to work on (in this case current directory `./`)
* --configfile - path to configuration file
* -j - number of cores to run on (in this case we will set it to 4)


Make sure that you have the conda environment `scrnaseq_course` activated. If you already have it active, no need to rerun. Let's test first with the `align` rule.

      export PATH=/proj/b2013006/nobackup/scrnaseq_course/pipeline/conda/bin:$PATH
      source activate scrnaseq_conda
      snakemake align -j 4 -np -d ./ --configfile config.yaml 

You should now see a list of all rules that the pipeline needs to run, and also all of the commands. It will also give a summary of the number of all rule counts.

Do the same but for everything including qc-report.

      snakemake make_qc_report -j 4 -np -d ./ --configfile config.yaml 


If all looks fine at this point, it is time to start running the pipeline. So all you have to do is to remove the -n flag and run again.

But since this will take quite a while to run, we suggest that you instead run the pipeline with an sbatch script. 

If you want to run instead as an sbatch job and submit it to the SLURM queue, there is an example sbatch script:

       /proj/b2013006/nobackup/scrnaseq_course/pipeline/run_all_pipeline.sbatch

Copy that file to your folder and change all paths for reports and working directory to your folder, and also create one folder for output of the sbatch reports and fix paths to that folder in the sbatch script. Also, do not forget to change the email adress where you will recieve Slurm notificiations. Then submit the job with:

      sbatch /proj/b2013006/nobackup/scrnaseq_course/pipeline/run_all_pipeline.sbatch     

Now you can check the queue and see if your job has started yet with:

    	jobinfo -u username

Take a look at the report files that you specified in your sbatch script. Any error message from the pipeline will be appear there, and you can also follow the progress of the pipeline. We expect that the pipeline should take about 1.5h to run, so you can proceed with other tutorials while it runs.

# Output files

If all went well, you should now have copleted all the steps. Within the folder `data/samples/` you should now have a number of files for each sample.

Have look in one of the sample folders and see if you can figure out what all the files are.

You should also have a number of summary files in the folder `data/sum/`.

* merge.rpkmforgenes_genes.txt - a list of genes and what transcripts are included per gene
* merge.rpkmforgenes_rpkm.txt - rpkms 
* merge.rpkmforgenes_counts.txt - counts
* merge.rpkmforgenes_rpkm.qc_stats.txt - qc-stats from rpkms
* qc_summary.csv - all qc stats 

plust 2 folders:
* qc_summary - contains the qc summary report and a list of suggested cells to filter out (if there are any cells that should be removed).
* multiqc - all multiqc files + the multiqc report.

Download the files `qc_summary.QC_report.html` and `multiqc_report.html` to your computer and have a look at them in your browser.

If you are happy with the filtering of cells, you can simply use the file that the qc-summary creates. You can also modify the filtering settings by making changes to the file qc_settings.yaml and rerun the step for creating qc-report with `-f` flag. 

In this small example, we only have 12 cells, so it does not make much sense do do filtering in this manner. You can instead have a look at the full summary from this project with 834 cells [here](https://export.uppmax.uu.se/b2017179/qc_reports/qc_summary.QC_report.html) or at uppmax at `/proj/b2013006/nobackup/scrnaseq_course/data/qc_summary.QC_report.html`. 

