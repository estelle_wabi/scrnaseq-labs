Example of scater package for QC
================================

Detailed tutorial of scater package at: <https://www.bioconductor.org/packages/release/bioc/vignettes/scater/inst/doc/vignette-qc.html>

We recommend that you follow steps 1-3 in the tutorial.

Many other packages builds on the SingleCellExperiment class in scater, so it is important that you learn properly how to create an SCE from your data and understand the basics of the scater package.

For this exercise you can either run with your own data or with the example data that they provide with the package. Below is an example with human innate lympoid cells (ILCs) from Bjorklund et al. 2016.

If you want to run the package with the ILCs, all data can be found at:

`/proj/b2013006/nobackup/scrnaseq_course/data/ILC`

OBS! As of July 2017, scater has switched from the SCESet class previously defined within the package to the more widely applicable SingleCellExperiment class. From Bioconductor 3.6 (October 2017), the release version of scater will use SingleCellExperiment.

### Load packages

``` r
suppressMessages(library(scater))
```

### Read data and create a scater SCESet

``` r
# read in meta data table and create pheno data
M <- read.table("data/ILC/Metadata_ILC.csv", sep=",",header=T)
# create and AnnotatedDataFrame - class from Biobase
pd <- new("AnnotatedDataFrame",data=M)
rownames(pd)<-M$Samples

# read rpkm values and counts
R <- read.table("data/ILC/ensembl_rpkmvalues_ILC.csv",sep=",",header=T)
C <- read.table("data/ILC/ensembl_countvalues_ILC.csv",sep=",",header=T)
```

Create the SCESet
-----------------

``` r
#example_sceset <- newSCESet(countData=C, phenoData = pd, logExprsOffset = 1, )
matC <- as.matrix(C)
example_sce <- SingleCellExperiment(assays = list(counts = matC), colData = M)

# you can also add in expression values from the rpkm matrix instead of using logged counts.
#set_exprs(example_sceset,"exprs") <- log2(as.matrix(R)+1)
exprs(example_sce) <- log2(as.matrix(R)+1)
assay(example_sce, "exprs") <- exprs(example_sce)

# you can access the rpkm or count matrix with the commands "counts" and "exprs"
counts(example_sce)[10:13,1:5]
```

    ##                 T74_P1_A9_ILC1 T74_P1_B4_NK T74_P1_B7_ILC2 T74_P1_B9_NK
    ## ENSG00000001167              0            0              0            0
    ## ENSG00000001460              0            0              0            0
    ## ENSG00000001461              0         1035              1            1
    ## ENSG00000001497              0            0              0            0
    ##                 T74_P1_D10_ILC2
    ## ENSG00000001167               0
    ## ENSG00000001460               0
    ## ENSG00000001461               2
    ## ENSG00000001497               0

``` r
exprs(example_sce)[10:13,1:5]
```

    ##                 T74_P1_A9_ILC1 T74_P1_B4_NK T74_P1_B7_ILC2 T74_P1_B9_NK
    ## ENSG00000001167              0     0.000000      0.0000000    0.0000000
    ## ENSG00000001460              0     0.000000      0.0000000    0.0000000
    ## ENSG00000001461              0     6.615791      0.2243554    0.2142426
    ## ENSG00000001497              0     0.000000      0.0000000    0.0000000
    ##                 T74_P1_D10_ILC2
    ## ENSG00000001167       0.0000000
    ## ENSG00000001460       0.0000000
    ## ENSG00000001461       0.8229705
    ## ENSG00000001497       0.0000000

We have accessor functions to access elements of the SingleCellExperiment object. \* counts(object): returns the matrix of read counts. As you can see above, if no counts are defined for the object, then the counts matrix slot is simpy NULL. \* exprs(object): returns the matrix of (log-counts) expression values, in fact accessing the logcounts slot of the object (synonym for logcounts).

For convenience (and backwards compatibility with SCESet) getters and setters are provided as follows: exprs, tpm, cpm, fpkm and versions of these with the prefix “norm\_”)

The closest to rpkms is in this case fpkms, so we use fpkm.

It also has slots for:

-   Cell metadata, which can be supplied as a DataFrame object, where rows are cells, and columns are cell attributes (such as cell type, culture condition, day captured, etc.).
-   Feature metadata, which can be supplied as a DataFrame object, where rows are features (e.g. genes), and columns are feature attributes, such as Ensembl ID, biotype, gc content, etc.

QC stats
--------

Use scater package to calculate qc-metrics

``` r
# first check which genes are spike-ins if you have included those
ercc <- grep("ERCC_",rownames(R))

# specify the ercc as feature control genes and calculate all qc-metrics
#isSpike(example_sce, "ERCC") <- ercc
#example_sce <- calculateQCMetrics(example_sce, feature_controls = "ERCC")
example_sce <- calculateQCMetrics(example_sce, feature_controls = list(set1= ercc[1]:ercc[length(ercc)]))

# check what all entries are - 
colnames(colData(example_sce))
```

    ##  [1] "Samples"                                   
    ##  [2] "Plate"                                     
    ##  [3] "Donor"                                     
    ##  [4] "Celltype"                                  
    ##  [5] "total_features"                            
    ##  [6] "log10_total_features"                      
    ##  [7] "total_counts"                              
    ##  [8] "log10_total_counts"                        
    ##  [9] "pct_counts_top_50_features"                
    ## [10] "pct_counts_top_100_features"               
    ## [11] "pct_counts_top_200_features"               
    ## [12] "pct_counts_top_500_features"               
    ## [13] "total_features_endogenous"                 
    ## [14] "log10_total_features_endogenous"           
    ## [15] "total_counts_endogenous"                   
    ## [16] "log10_total_counts_endogenous"             
    ## [17] "pct_counts_endogenous"                     
    ## [18] "pct_counts_top_50_features_endogenous"     
    ## [19] "pct_counts_top_100_features_endogenous"    
    ## [20] "pct_counts_top_200_features_endogenous"    
    ## [21] "pct_counts_top_500_features_endogenous"    
    ## [22] "total_features_feature_control"            
    ## [23] "log10_total_features_feature_control"      
    ## [24] "total_counts_feature_control"              
    ## [25] "log10_total_counts_feature_control"        
    ## [26] "pct_counts_feature_control"                
    ## [27] "pct_counts_top_50_features_feature_control"
    ## [28] "total_features_set1"                       
    ## [29] "log10_total_features_set1"                 
    ## [30] "total_counts_set1"                         
    ## [31] "log10_total_counts_set1"                   
    ## [32] "pct_counts_set1"                           
    ## [33] "pct_counts_top_50_features_set1"           
    ## [34] "is_cell_control"

A more detailed description can be found at the tutorial site, or by running: `?calculateQCMetrics`

If you have additional qc-metrics that you want to include, like mapping stats, rseqc data etc, you can include all of that in your phenoData.

Look at data interactively in GUI
---------------------------------

You can play around with the data interactively with the shiny app they provide. OBS! It takes a while to load and plot, so be patient.

``` r
# you can open the interactive gui with:
scater_gui(example_sce)
```

Plots of expression values
--------------------------

Different ways of visualizing gene expression per batch/celltype etc.

``` r
# plot detected genes at different library depth for different plates and celltypes
plotScater(example_sce, block1 = "Plate", block2 = "Celltype",
     colour_by = "Celltype", nfeatures = 300, exprs_values = "exprs")
```

![](scater_ilc_files/figure-markdown_github/unnamed-chunk-6-1.png)

``` r
# violin plot for gene expression
plotExpression(example_sce, rownames(example_sce)[6:11],
               x = "Celltype", exprs_values = "exprs", colour = "Donor",log=TRUE)
```

![](scater_ilc_files/figure-markdown_github/unnamed-chunk-6-2.png)

``` r
plotExpression(example_sce, rownames(example_sce)[6:11],
               x = "Celltype", exprs_values = "counts", colour = "Donor",
               show_median = TRUE, show_violin = FALSE,  log = TRUE)
```

![](scater_ilc_files/figure-markdown_github/unnamed-chunk-6-3.png)

You can play around with all the arguments in plotExpression, for example:

-   log=TRUE/FALSE
-   show\_violin=TRUE/FALSE
-   show\_median=TRUE/FALSE
-   exprs\_values="counts"/"exprs"

And specify different coloring and and batches to plot by that are defined in the CellMetadata (ex-phenoData in the SCESet class).

QC overview and filtering
-------------------------

There are several ways to plot the QC summaries of the cells in the scater package. A few examples are provided below. In this case, cells have already been filtered to remove low quality samples, so no filtering step is performed.

``` r
# first remove all features with no/low expression, here set to expression in more than 5 cells with > 1 count
keep_feature <- rowSums(counts(example_sce) > 1) > 5
example_sce <- example_sce[keep_feature,]

## Plot highest expressed genes.
plotQC(example_sce, type = "highest-expression",col_by="Celltype")
```

![](scater_ilc_files/figure-markdown_github/unnamed-chunk-7-1.png)

Plot frequency of expression (number of cells with detection) vs mean normalised expression.

``` r
#plotQC(example_sce, type = "exprs-freq-vs-mean")
plotExprsFreqVsMean(example_sce)
```

    ## `geom_smooth()` using method = 'loess'

![](scater_ilc_files/figure-markdown_github/unnamed-chunk-8-1.png)

Plot log10 total count vs number of cells a gene is detected in.

``` r
plotFeatureData(example_sce, aesth = aes_string(x = "n_cells_counts", y =
"log10_total_counts"))
```

![](scater_ilc_files/figure-markdown_github/unnamed-chunk-9-1.png)

Plot different qc-metrics per batch.

``` r
p1 <- plotPhenoData(example_sce, aes(x = Donor, y = total_features,                                  colour = log10_total_counts))

p2 <- plotPhenoData(example_sce, aes(x = Celltype, y = total_features,                                  colour = log10_total_counts))

multiplot(p1, p2, rows = 2)
```

![](scater_ilc_files/figure-markdown_github/unnamed-chunk-10-1.png)

    ## [1] 2

Plot the percentage of expression accounted for by feature controls against total\_features.

``` r
plotPhenoData(example_sce, 
      aes(x = total_features, y = pct_counts_feature_control, colour = Donor)) + 
      theme(legend.position = "top") +
      stat_smooth(method = "lm", se = FALSE, size = 2, fullrange = TRUE)
```

![](scater_ilc_files/figure-markdown_github/unnamed-chunk-11-1.png)

Dimensionality reduction plots
------------------------------

Plot the cells in reduced space and define color/shape/size by different qc-metrics or meta-data entries.

``` r
# PCA - with different coloring by celltype and donor, first 4 components
plotPCA(example_sce,ncomponents=4,colour_by="Celltype",shape_by="Donor")
```

![](scater_ilc_files/figure-markdown_github/unnamed-chunk-12-1.png)

``` r
plotPCA(example_sce,ncomponents=4,colour_by="Donor",shape_by="Celltype")
```

![](scater_ilc_files/figure-markdown_github/unnamed-chunk-12-2.png)

``` r
# Diffusion map 
plotDiffusionMap(example_sce, colour_by="Celltype",shape_by="Donor",ncomponents=4)
```

![](scater_ilc_files/figure-markdown_github/unnamed-chunk-12-3.png)

``` r
# tSNE - uses Rtsne function to run tsne
plotTSNE(example_sce, colour_by="Celltype",shape_by="Donor", ntop=30, perplexity=30 )
```

![](scater_ilc_files/figure-markdown_github/unnamed-chunk-12-4.png)

For all of these dimensionality reduction methods, you can specify `return_SCE = TRUE` and it will return an SCESet object with the slot reducedDimension filled. This can be usefule if PCA/tSNE takes long time to run and you want to plot several different colors etc.

You can later plot the reduced dimension with `plotReducedDim`.

### PCA based on QC-measures

PCA based on the phenoData can be used to detect outlier cells with qc-measures that deviates from the rest. But be careful with checking how these cells deviate before taking a decision on why to remove them.

OBS! detection of outlier requires that package `mvoutlier` is installed.

``` r
example_sce <- plotPCA(example_sce, pca_data_input = "pdata", 
                          detect_outliers = TRUE, return_SCE = TRUE)
```

    ## The following selected_variables were not found in colData(object): pct_counts_feature_controlsThe following selected_variables were not found in colData(object): total_features_feature_controlsThe following selected_variables were not found in colData(object): log10_total_counts_feature_controls

    ## Other variables from colData(object) can be used by specifying a vector of variable names as the selected_variables argument.

    ## PCA is being conducted using the following variables:pct_counts_top_100_featurestotal_featureslog10_total_counts_endogenous

    ## sROC 0.1-2 loaded

![](scater_ilc_files/figure-markdown_github/unnamed-chunk-13-1.png)

``` r
# we can use the filter function to remove all outlier cells
filtered_sce <- filter(example_sce, outlier==FALSE)
```

QC of experimental variables
----------------------------

Median marginal R2 for each variable in pData(example\_sceset) when fitting a linear model regressing exprs values against just that variable. Shows how much of the data variation is explained by a single variable.

``` r
plotQC(example_sce, type = "expl")
```

    ## The variable pct_counts_top_50_features_feature_control only has one unique value, so R^2 is not meaningful.
    ## This variable will not be plotted.

    ## The variable pct_counts_top_50_features_set1 only has one unique value, so R^2 is not meaningful.
    ## This variable will not be plotted.

    ## The variable is_cell_control only has one unique value, so R^2 is not meaningful.
    ## This variable will not be plotted.

    ## The variable outlier only has one unique value, so R^2 is not meaningful.
    ## This variable will not be plotted.

![](scater_ilc_files/figure-markdown_github/unnamed-chunk-14-1.png)

Identify PCs that correlate strongly to certain QC or Meta-data values

``` r
# for total_features
plotQC(example_sce, type = "find-pcs", variable = "total_features", plot_type = "pairs-pcs")
```

![](scater_ilc_files/figure-markdown_github/unnamed-chunk-15-1.png)

``` r
# for Donor
plotQC(example_sce, type = "find-pcs", variable = "Donor", plot_type = "pairs-pcs")
```

![](scater_ilc_files/figure-markdown_github/unnamed-chunk-15-2.png)

PC1 clearly correlates to total\_features, which is a common problem in scRNAseq data. This may be a technical artifact, or a biological features of celltypes with very different sizes.

It is also clear that PC1 separates out the different donors.

\`\`\`
