biomaRt
=======

Here is some example code on how to translate between gene symbols and ensembl gene ids using the biomaRt package. For more details on the package, have a look at: <https://bioconductor.org/packages/release/bioc/html/biomaRt.html>

All data you need is available in the folder: `/proj/b2013006/nobackup/scrnaseq_course/data/ILC/`

``` r
suppressMessages(require(biomaRt))

# select which mart to use, in this case ensembl
mart <- useMart("ensembl")

# To see what datasets exits you can run: listDatasets
head(listDatasets(mart))
```

    ##                          dataset                    description
    ## 1      ptroglodytes_gene_ensembl  Chimpanzee genes (CHIMP2.1.4)
    ## 2           panubis_gene_ensembl Olive baboon genes (PapAnu2.0)
    ## 3        ttruncatus_gene_ensembl        Dolphin genes (turTru1)
    ## 4    aplatyrhynchos_gene_ensembl      Duck genes (BGI_duck_1.0)
    ## 5 itridecemlineatus_gene_ensembl     Squirrel genes (SpeTri2.0)
    ## 6        ocuniculus_gene_ensembl       Rabbit genes (OryCun2.0)
    ##        version
    ## 1   CHIMP2.1.4
    ## 2    PapAnu2.0
    ## 3      turTru1
    ## 4 BGI_duck_1.0
    ## 5    SpeTri2.0
    ## 6    OryCun2.0

``` r
# in this case we use hsapiens_gene_ensembl
mart <- useDataset("hsapiens_gene_ensembl", mart = mart)
```

Search based on Ensembl ID
--------------------------

Here we will get gene\_id, gene\_name, description and biotype for all ensembl\_ids that we have in the expression matrix.

``` r
# to find out what attributes there are in the Dataset, use listAttributes
head(listAttributes(mart))
```

    ##                            name                  description         page
    ## 1               ensembl_gene_id               Gene stable ID feature_page
    ## 2       ensembl_gene_id_version       Gene stable ID version feature_page
    ## 3         ensembl_transcript_id         Transcript stable ID feature_page
    ## 4 ensembl_transcript_id_version Transcript stable ID version feature_page
    ## 5            ensembl_peptide_id            Protein stable ID feature_page
    ## 6    ensembl_peptide_id_version    Protein stable ID version feature_page

``` r
# read in expression matrix to get the genes we want to translate
R <- read.table("data/ILC/ensembl_rpkmvalues_ILC.csv",sep=",",header=T)

# getBM function fetches attributes from the database with specified names. 
# with filters parameter you define which attribute you want to filter on
# with values, you define which entries you want to fetch, leave empty to fetch all entries.
# with attributes, you define what attributes you want to fetch

genes.table <- getBM(filters= "ensembl_gene_id", attributes= c("ensembl_gene_id", "external_gene_name", "description","gene_biotype"), values= rownames(R), mart= mart) 

head(genes.table)
```

    ##   ensembl_gene_id external_gene_name
    ## 1 ENSG00000251931          RNU6-871P
    ## 2 ENSG00000207766             MIR626
    ## 3 ENSG00000207260           RNU6-35P
    ## 4 ENSG00000265993            MIR5694
    ## 5 ENSG00000207185         RNU6-1157P
    ## 6 ENSG00000201545           RNU4-85P
    ##                                                                  description
    ## 1  RNA, U6 small nuclear 871, pseudogene [Source:HGNC Symbol;Acc:HGNC:47834]
    ## 2                           microRNA 626 [Source:HGNC Symbol;Acc:HGNC:32882]
    ## 3   RNA, U6 small nuclear 35, pseudogene [Source:HGNC Symbol;Acc:HGNC:34279]
    ## 4                          microRNA 5694 [Source:HGNC Symbol;Acc:HGNC:43530]
    ## 5 RNA, U6 small nuclear 1157, pseudogene [Source:HGNC Symbol;Acc:HGNC:48120]
    ## 6   RNA, U4 small nuclear 85, pseudogene [Source:HGNC Symbol;Acc:HGNC:47021]
    ##   gene_biotype
    ## 1        snRNA
    ## 2        miRNA
    ## 3        snRNA
    ## 4        miRNA
    ## 5        snRNA
    ## 6        snRNA

``` r
# write to a file for later use
write.table(genes.table, file="data/ILC/gene_name_translation_biotype.tab",sep="\t")
```

Fetch Ensembl ID based on gene names
------------------------------------

You can do the opposite if you have gene names and want Ensembl IDs.

``` r
# now we want to get all ensembl IDs for the genes in genes.table$external_gene_name
genes.table2 <- getBM(filters= "external_gene_name", attributes= c("ensembl_gene_id", "external_gene_name", "description","gene_biotype"), values= genes.table$external_gene_name, mart= mart)

# Keep in mind, you may get multiple ensembl IDs translated to the same gene name, so the number of entries will be different.
dim(genes.table)
```

    ## [1] 49087     4

``` r
dim(genes.table2)
```

    ## [1] 53319     4

Also, keep in mind that if you are working with an older version of Ensembl, some Ensembl IDs may be obsolete and not have any translation, so those will require some manual searching to annotate with gene names.

Fetch Gene Ontology annotations
-------------------------------

You can also use biomaRt to fetch gene ontology annotations and a bunch of other attributes that you can find in the database. Here is an example for fetching GO-terms, that may be useful for running Pagoda if you are using your own dataset.

``` r
go.table <- getBM(filters= "ensembl_gene_id", attributes= c("ensembl_gene_id", "external_gene_name", "go_id","name_1006", "namespace_1003"), values= rownames(R), mart= mart)

head(go.table)
```

    ##   ensembl_gene_id external_gene_name      go_id
    ## 1 ENSG00000012983             MAP4K5           
    ## 2 ENSG00000012983             MAP4K5 GO:0005622
    ## 3 ENSG00000012983             MAP4K5 GO:0000166
    ## 4 ENSG00000012983             MAP4K5 GO:0004672
    ## 5 ENSG00000012983             MAP4K5 GO:0004674
    ## 6 ENSG00000012983             MAP4K5 GO:0005524
    ##                                  name_1006     namespace_1003
    ## 1                                                            
    ## 2                            intracellular cellular_component
    ## 3                       nucleotide binding molecular_function
    ## 4                  protein kinase activity molecular_function
    ## 5 protein serine/threonine kinase activity molecular_function
    ## 6                              ATP binding molecular_function

``` r
# If you want to create a list with all genes as keys, and a vector of go-terms as values
gene2go <- split(go.table$go_id, go.table$ensembl_gene_id)
head(gene2go)
```

    ## $ENSG00000000003
    ##  [1] "GO:0016020" "GO:0016021" "GO:0004871" "GO:0005887" "GO:0005515"
    ##  [6] "GO:0070062" "GO:0007166" "GO:0043123" "GO:0039532" "GO:1901223"
    ## [11] ""          
    ## 
    ## $ENSG00000000005
    ##  [1] "GO:0005634" "GO:0005737" "GO:0016020" "GO:0016021" "GO:0005515"
    ##  [6] "GO:0005635" "GO:0001886" "GO:0001937" "GO:0016525" "GO:0035990"
    ## [11] "GO:0071773" ""          
    ## 
    ## $ENSG00000000419
    ##  [1] "GO:0005634" "GO:0016020" "GO:0016740" "GO:0016757" "GO:0005783"
    ##  [6] "GO:0005515" "GO:0006486" "GO:0006506" "GO:0005789" "GO:0004169"
    ## [11] "GO:0004582" "GO:0018279" "GO:0019348" "GO:0035268" "GO:0035269"
    ## [16] "GO:0033185" ""           "GO:0097502" "GO:0005537" "GO:0043178"
    ## [21] "GO:0019673" "GO:0043231"
    ## 
    ## $ENSG00000000457
    ##  [1] "GO:0005737" "GO:0005524" "GO:0006468" "GO:0005794" "GO:0042995"
    ##  [6] "GO:0005515" "GO:0030027" "GO:0016477" "GO:0004672" ""          
    ## 
    ## $ENSG00000000460
    ## [1] ""
    ## 
    ## $ENSG00000000938
    ##  [1] "GO:0005737" "GO:0016020" "GO:0000166" "GO:0004672" "GO:0005524"
    ##  [6] "GO:0006468" "GO:0016740" "GO:0016301" "GO:0016310" "GO:0005743"
    ## [11] "GO:0004713" "GO:0005886" "GO:0005829" "GO:0005739" "GO:0005856"
    ## [16] "GO:0042995" "GO:0005515" "GO:0016235" "GO:0004715" "GO:0007229"
    ## [21] "GO:0002376" "GO:0045087" "GO:0038096" "GO:0005576" "GO:0005758"
    ## [26] "GO:0070062" "GO:0019901" "GO:0032587" "GO:0030335" "GO:0030154"
    ## [31] "GO:0050830" "GO:0043312" "GO:0008360" "GO:0042127" "GO:0034774"
    ## [36] "GO:0046777" "GO:0016477" "GO:0001784" "GO:0009615" "GO:0045859"
    ## [41] "GO:0015629" "GO:0014068" "GO:0050764" "GO:0038083" "GO:0031234"
    ## [46] "GO:0007169" "GO:0018108" "GO:0043552" "GO:0050715" "GO:0034987"
    ## [51] "GO:0002768" "GO:0034988" "GO:0043306" "GO:0045088" ""

``` r
# To do the opposite, go-terms as keys with a vector of genes with that go-term
go2gene <- split(go.table$ensembl_gene_id, go.table$go_id)
```

Select only Biological Process
------------------------------

If you want to select only Biological Process, the entry "namespace\_1003" defines the type of GO-term, so you can filter on that as well.

``` r
go.tableBP <- go.table[go.table$namespace_1003=="biological_process",]

# if you want more informative names for the go-terms, merge GO-id with name
go.name <- paste(go.tableBP$go_id,go.tableBP$name_1006,sep=";")
go.tableBP$go.name <- go.name

#make a list with GO-name to gene IDs
goBP2gene <- split(go.tableBP$ensembl_gene_id, go.tableBP$go.name)

# save to file
save(goBP2gene, file="data/ILC/GO_BP_annotations.Rdata")
```
